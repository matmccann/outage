<?php

/**
 * Ansible managed
 * Changes to this file may be overwritten.
 * Put customizations in settings.local.php instead.
 */

$local_settings = dirname(__FILE__) . '/settings.local.php';
if (file_exists($local_settings)) {
    include $local_settings;
}
