<?php
/**
 * Outage Tool control panel report generator
 */
// set the default time zone
date_default_timezone_set('America/Toronto');


$path = '/home/www/outage-php/wwwroot/';
set_include_path(get_include_path() . PATH_SEPARATOR . $path);
require_once('class.phpmailer.php');
require_once('../settings.php');
$mail = new PHPMailer();
date_default_timezone_set('EST');
$mysqli = new mysqli($DBHOST,$DBUSER, $DBPASS, $DBNAME);

// Check if the webform is enabled for cron
	if ($argv[1] !== 'force') { // If someone wants to manually run the report
		$result = $mysqli->query("SELECT `value` FROM webformControl WHERE `setting` = 'WebformStatus'");
		$row = $result->fetch_object();
		if ($row->value !== 'On') {
			return;
		}
	}

$result = $mysqli->query("SELECT `value` FROM webformControl WHERE `setting` = 'reportEmails'");
$data = $result->fetch_object();
$data = $data->value;
$data = unserialize($data);

$to = $data;

$body = '';
$body .= '<font style="color:#666666; font-face: arial; font-size: 12px;">';
$body .= '<h1 style="font-size: 20px; font-weight: bold; color: #92D050; padding-bottom: 25px;">Outage report for: ' . date("F j, Y, g:i a") . ' (EDT)</h1>';
$body .= '<h2 style="color: #49166D; font-size: 17px;">Client submissions</h2>';
$body .= '<table cellpadding="0" cellspacing="0" style="border-collapse: collapse;border: 1px solid #CCCCCC; padding: 5px; -moz-box-shadow: 0px 2px 2px 2px #ccc; -webkit-box-shadow: 0px 2px 2px 2px #ccc; box-shadow: 0px 2px 2px 2px #ccc; border-radius: 5px;">';
$body .= '<tr>';
$body .= '<td style="font-weight: bold; width: 150px; padding: 5px 0px 5px 10px; border-bottom: 1px solid #CCCCCC;">Total</td>';

$result = $mysqli->query("SELECT COUNT(id) AS num FROM webformResults");
$row = $result->fetch_object();
$body .= '<td style="font-weight: bold; width: 50px; text-align: center; border-left: 1px solid #CCCCCC; border-bottom: 1px solid #CCCCCC; padding: 5px 0px 5px 0px;">' . $row->num . '</td>';

$body .= '</tr>';
$body .= '</table>';
$body .= '</font>';

$body = preg_replace("/\\\\/",'',$body);

$mail->SetFrom('noreply@client-name.client-name.com', 'client-name');
foreach($to as $value) {
  $mail->AddAddress($value);
}
$mail->Subject = "Outage report for " . date("F j, Y, g:i a") . ' (EST)';
$mail->AltBody = "To view the message, please use an HTML compatible email viewer!";
$mail->MsgHTML($body);
$mail->Encoding = 'base64';

if(!$mail->Send()) {
  error_log("Mailer Error: " . $mail->ErrorInfo);
}


// Update Status in Database
$result = $mysqli->query("REPLACE INTO `webformControl` (`setting`,`value`) VALUES ('ReportLastSent', NOW())");
if ($argv[1] !== 'force') { // If someone wants to manually run the report
	$result = $mysqli->query("REPLACE INTO `webformControl` (`setting`,`value`) VALUES ('ReportLastSentStatus', 'Auto')");
} else {
	$result = $mysqli->query("REPLACE INTO `webformControl` (`setting`,`value`) VALUES ('ReportLastSentStatus', 'Manual')");
}

$mysqli->close();
?>
