<?php

// Include $production variable.
include('config.php');

/**
 * Outage Tool bulk mail sending script
 */
// set the default time zone
date_default_timezone_set('America/Toronto');
$_SERVER['DOCUMENT_ROOT'] = dirname(dirname(__FILE__)) . '/wwwroot';
set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);
require_once($_SERVER['DOCUMENT_ROOT'].'/includes/common.inc');
require_once($_SERVER['DOCUMENT_ROOT'].'/includes/language.inc');
require_once(dirname($_SERVER['DOCUMENT_ROOT']).'/settings.php');
$mysqli = new mysqli($DBHOST,$DBUSER, $DBPASS, $DBNAME);


$fName = '/home/www/outage-php/keys/webform.key';
$f = fopen($fName, 'r');
$key = fread($f, filesize($fName));

//get the settings
$result = $mysqli->query("SELECT `value` FROM webformControl WHERE `setting` = 'emailLastSent'");
$emailLastSent = $result->fetch_object()->value;
$result = $mysqli->query("SELECT `value` FROM webformControl WHERE `setting` = 'emailQuantity'");
$emailQuantity = $result->fetch_object()->value;
$result = $mysqli->query("SELECT `value` FROM webformControl WHERE `setting` = 'emailFrequency'");
$emailFrequency = $result->fetch_object()->value;




//Check if we're meant to send right now
if ($argv[1] !== 'force') {
  //First check that we're sending
  $result = $mysqli->query("SELECT `value` FROM webformControl WHERE `setting` = 'emailStatus'");
  $emailStatus = $result->fetch_object()->value;
  if($emailStatus != 1){
    return;
  }
  //Ok, we're sending, now check the time
  $now = time();
  $nextRun = strtotime($emailLastSent .' + '.$emailFrequency.' minutes');
  if($now <= $nextRun){
    return;
  }
}
else{
  //if forced, should set to sending
  $result = $mysqli->query("REPLACE INTO `webformControl` (`setting`,`value`) VALUES ('emailStatus', 1)");
}

// Is there anyone left to send to? if so, we'll send to these people if everything else passes
// PRODUCTION query is different from that of STAGING
if ($production==true){
  $sendResult = $mysqli->query("SELECT email, language, unix_timestamp(timestamp) as timestamp FROM webformResults WHERE notified = 0 ORDER by id LIMIT $emailQuantity");
}
else {
  $sendResult = $mysqli->query("SELECT id, AES_DECRYPT(email, '$key') as email, language FROM webformResults WHERE notified = 0 ORDER by id LIMIT $emailQuantity");
}

if(!$sendResult->num_rows){
  //email sending is complete.
  $update = $mysqli->query("REPLACE INTO `webformControl` (`setting`,`value`) VALUES ('emailStatus', 3)");
  return;
}

$count = 1;
$english_email = array();
$french_email = array();
while($row = $sendResult->fetch_assoc()) {
  if($row['language'] == 'en'){
    $english_email[$row['id']] = $row['email'];
  }
  else{
    $french_email[$row['id']] = $row['email'];
  }

  if($count == 500){ //Send every 500 to keep load on Sendgrid down (250 tops)
    if(!empty($english_email) && sendOutageOverEmail($english_email, 'en')){
      $query = "UPDATE webformResults SET notified = 1 WHERE id IN(".implode(',',array_keys($english_email)).")";
      $mysqli->query($query);
      $update = $mysqli->query("REPLACE INTO `webformControl` (`setting`,`value`) VALUES ('emailLastSent', NOW())");
    }
    if(!empty($french_email) && sendOutageOverEmail($french_email, 'fr')){
      $query = "UPDATE webformResults SET notified = 1 WHERE id IN(".implode(',',array_keys($french_email)).")";
      $mysqli->query($query);
      $update = $mysqli->query("REPLACE INTO `webformControl` (`setting`,`value`) VALUES ('emailLastSent', NOW())");
    }
    //reset
    $count = 0;
    $english_email = array();
    $french_email = array();
  }
  $count++;
}
//Send any remainders
if(!empty($english_email) && sendOutageOverEmail($english_email, 'en')){
    $query = "UPDATE webformResults SET notified = 1 WHERE id IN(".implode(',',array_keys($english_email)).")";
    $mysqli->query($query);
    $update = $mysqli->query("REPLACE INTO `webformControl` (`setting`,`value`) VALUES ('emailLastSent', NOW())");
}
if(!empty($french_email) && sendOutageOverEmail($french_email, 'fr')){
    $query = "UPDATE webformResults SET notified = 1 WHERE id IN(".implode(',',array_keys($french_email)).")";
    $mysqli->query($query);
    $update = $mysqli->query("REPLACE INTO `webformControl` (`setting`,`value`) VALUES ('emailLastSent', NOW())");
}

// Final check to set to complete if nobody is left
// PRODUCTION query is different from that of STAGING
if ($production==true){
  $endResult = $mysqli->query("SELECT id, email, language, FROM webformResults WHERE notified = 0 ORDER by id LIMIT $emailQuantity");
}
else {
  $endResult = $mysqli->query("SELECT id, AES_DECRYPT(email, '$key') as email, language FROM webformResults WHERE notified = 0 ORDER by id LIMIT $emailQuantity");
}

if(!$endResult->num_rows){
  $update = $mysqli->query("REPLACE INTO `webformControl` (`setting`,`value`) VALUES ('emailStatus', 3)");
}

function sendOutageOverEmail($recipients, $language) {
  require_once('../wwwroot/includes/language.inc');
  $url = 'https://sendgrid.com/';
  $user = 'user';
  $pass = 'password';

  $GLOBALS['t']->setLocale($language);

  $template = new Templater();
	$template->setTemplateDir('templates', TRUE);
  $template->setTemplate('TEMPLATE', $language);
  $templateData = array(
    'logoImage' => $GLOBALS['t']->_("https://shop.client-name.com/outage-php/images/logo.gif"),
		'headline' => $GLOBALS['t']->_("My Account is back"),
    // use this paragraph1 after the august 1st outage is finished
//		'paragraph1' => $GLOBALS['t']->_("We're pleased to let you know that online access has been restored. We regret any inconvenience and appreciate your patience."),
		'paragraph1' => $GLOBALS['t']->_("We're pleased to let you know that online account access has been restored. Please note that delays in billing and Internet usage details may still occur. We appreciate your patience."),
		'url1' => $GLOBALS['t']->_("Log in now"),
		'signature1' => $GLOBALS['t']->_("Thanks,"),
		'signature2' => $GLOBALS['t']->_("The client-name Team"),
		'footerTopURL1' => $GLOBALS['t']->_("Search on client-name.com"),
		'footerTopURL2' => $GLOBALS['t']->_("Get Help"),
		'legal1' => $GLOBALS['t']->_("We respect your privacy and will not provide your personal information to other parties without your consent."),
		'legal2' => $GLOBALS['t']->_("Please do not reply to this message. Replies to the email address are not monitored by client-name. If you would like to contact us, please click below."),
		'footerURL1' => $GLOBALS['t']->_("Privacy statement"),
    'footerURL2' => $GLOBALS['t']->_("Contact us"),
    'footerURL3' => $GLOBALS['t']->_("Where to buy"),
    'footerURL4' => $GLOBALS['t']->_("client-name.com"),
    'copy' => $GLOBALS['t']->_('client name company'),
  );
  $template->setData($templateData);
  $template->load();

	$body = $template->getOutput();

	$body = preg_replace("/\\\\/",'',$body);

  $json_string = array(
    'to' => array_values($recipients),
    'category' => 'outagecentre',
  );

  $params = array(
      'api_user'  => $user,
      'api_key'   => $pass,
      'x-smtpapi' => json_encode($json_string),
      'to'        => 'webmaster@acromediainc.com',
      'subject'   => $GLOBALS['t']->_("My client-name access is now available"),
      'html'      => $body,
      'fromname'  => $GLOBALS['t']->_("client-name Maintenance Team"),
      'from'      => 'donotreply@client-name.com',
    );

  $request =  $url.'api/mail.send.json';

  // Generate curl request
  $session = curl_init($request);
  // Tell curl to use HTTP POST
  curl_setopt ($session, CURLOPT_POST, true);
  // Tell curl that this is the body of the POST
  curl_setopt ($session, CURLOPT_POSTFIELDS, $params);
  // Tell curl not to return headers, but do return the response
  curl_setopt($session, CURLOPT_HEADER, false);
  curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($session, CURLOPT_FOLLOWLOCATION, true);

  // obtain response
  $response = curl_exec($session);
  curl_close($session);
  $response = json_decode($response);
  if($response->message == 'success'){
    return TRUE;
  }
  if ($response->errors) {
    error_log("Sendgrid returned one or more errors:");
    try {
      error_log(print_r($response->errors, true));
    } catch (Exception $exc) {
      error_log($exc->getTraceAsString());
    }
  }
  return FALSE;
}

?>
