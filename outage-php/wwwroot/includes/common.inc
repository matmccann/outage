<?php

require_once('../settings.php');
if (!isset($DBHOST) || empty($DBHOST)) {
  $DBHOST="localhost";
}

class Templater {

  private $output;
  private $template_dir;
  private $template_name;
  private $template_file;
  private $data;
  private $errors;

  public $is_error;

  public function __construct() {
    $this->output = '';
    $this->template_dir = 'templates';
    $this->template_name = '';
    $this->data = array();
    $this->errors = array();
    $this->is_error = false;
  }

  public function setTemplate($name, $language) {
    $this->template_name = $name;
    if ($name === 'MyTELUS') {
      if($language === 'fr') {
        $this->template_file = $this->template_dir . '/' . $this->template_name . '_FR.HTML';
      }
      else {
        $this->template_file = $this->template_dir . '/' . $this->template_name . '.HTML';
      }

    }
    else {
      $this->template_file = $this->template_dir . '/' . $this->template_name . '.inc';
    }
  }

  public function setTemplateDir($dir, $html=FALSE) {
    $this->template_dir = $dir;
    if($this->template_name != '') {
      if ($html) {
        $this->template_file = $this->template_dir . '/' . $this->template_name . '.HTML';
      }
      else{
        $this->template_file = $this->template_dir . '/' . $this->template_name . '.inc';
      }
    }
  }

  public function setData($data) {
    if(is_array($data)) {
      $this->data = $data;
    }
    else {
      $this->setError('Data must be an array');
    }
  }

  public function load() {
    if(!$this->is_error) {
      foreach($this->data as $key => $value) {
        $$key = $value;
      }
      ob_start();
      include($this->template_file);
      $this->output = ob_get_clean();
    }
    else {
      $this->output = 'There are errors...';
    }
  }

  public function getOutput() {
    return $this->output;
  }

  private function setError($string) {
    $this->errors[] = $string;
    if($this->is_error == false) {
      $this->is_error = true;
    }
  }

  public function getErrors() {
    if(count($this->errors) > 0) {
      $return = '<ul class="errors">';
      foreach($this->errors as $error) {
        $return .= '<li>' . $error . '</li>';
      }
      $return .= '</ul>';
    }
    else {
      $return = 'There are no errors';
    }
    return $return;
  }
}

function lang($en, $fr) {


}

/*
 *
 * This function can be used to mask phone numbers
 *
 */
function maskPhone($mobileNumber) {
	$mobileNumber = substr_replace($mobileNumber, 'XXXXXX', 0, 6);
	$mobileNumber = substr_replace($mobileNumber, '-', 3, 0);
	$mobileNumber = substr_replace($mobileNumber, '-', 7, 0);
	return $mobileNumber;
}
function formatPhone($mobileNumber) {
	$mobileNumber = substr_replace($mobileNumber, '-', 3, 0);
	$mobileNumber = substr_replace($mobileNumber, '-', 7, 0);
	return $mobileNumber;
}

function checkToolStatus() {
  global $DBHOST, $DBUSER, $DBPASS, $DBNAME;
  $mysqli = new mysqli($DBHOST,$DBUSER, $DBPASS, $DBNAME);

  $query = 'SELECT value FROM webformControl WHERE setting = "WebformStatus"';
  $result = $mysqli->query($query);
  $row = $result->fetch_assoc();

  if($row['value'] == 'On') {
    return TRUE;
  }

  return FALSE;
}

?>
