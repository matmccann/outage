<?php

// *********** Configure the PRODUCTION redirect ONLY on production server ***********
$production = false;

/**
 * Invalid/unrecognized "lang" arguments cause a 500 error.
 * We need to fix that before anything else can happen.
 */
if ($_GET['lang'] == 'ENGLISH') {
  header('Location: index.php?lang=en');
  exit(0);
}
else if ($_GET['lang'] == 'FRENCH') {
  header('Location: index.php?lang=fr');
  exit(0);
}
else if ($_GET['lang'] == 'en') {
  /**
   * This is fine.
   */
}
else if ($_GET['lang'] == 'fr') {
  /**
   * This is fine.
   */
}
else if (!isset($_GET['lang'])) {
  /**
   * This is fine.
   */
}
else {
  /**
   * All other undefined 'lang' values will cause a 500 error.
   * Default to 'en' and let the user choose 'fr' themselves from the page if they don't like it.
   */
  header('Location: index.php?lang=en');
  exit(0);
}



session_start();
header('Cache-control: no-store');
require_once('includes/common.inc');
require_once('includes/rfc822.inc');

// set the default time zone
date_default_timezone_set('America/Toronto');

include_once('includes/language.inc');

//if the offline file is there, the outage tool is turned off
if(!checkToolStatus()) {
  header('location: offline.php');
}

$hasError = false;

if(!$_POST) {
  $stage = 1;
}
if($_POST) {
  $stage = 1;
  if(validateStage1()) {
    $stage = 2;
  }
}

if($stage == 1) {
  $_SESSION['webform_inserted'] = FALSE;
}

function validateStage1() {
  global $hasError, $errorMessage;
  if(!is_valid_email_address($_POST['email'])) {
    $hasError = true;
    $errorMessage = $GLOBALS['t']->_("Please enter a valid email.");
  }

  if(!$hasError) {
    return true;
  }
  else {
    return false;
  }
}


if($stage == 2) {
  if(!$_SESSION['webform_inserted']) {
    //Correct key might be the ones in the preorder folder, not outage
    $f = fopen($fName, 'r');
    $key = fread($f, filesize($fName));

    $mysqli = new mysqli($DBHOST, $DBUSER, $DBPASS, $DBNAME);

    $args = $_POST;
    $args['timestamp'] = date("Y-m-d H:i:s");

    /* Sanitize the values */
    foreach($args as $array_key => $value) {
      $args[$array_key] = $mysqli->real_escape_string($value);
    }

    unset($args['viewing_language']);

    $query = vsprintf("INSERT INTO webformResults (id, email, language, timestamp) VALUES(0, AES_ENCRYPT('%s', '$key'), '%s', '%s')", $args);

    if($mysqli->query($query)){
      $_SESSION['webform_inserted'] = TRUE;
      header("location:thank-you.php");
    }
    else{
      $hasError = true;
      $errorMessage = $GLOBALS['t']->_("Email address already submitted.");
    }
    $mysqli->close();
  }
}

/**
 * Redirect to CLIENT hosted email form IF IN PRODUCTION!.
 */
if($production===true) {
  $lang = 'en';
  if($_GET['lang']){
    $lang = $_GET['lang'];
  }

  if(!checkToolStatus()) {
    $redirect_url = "https://client-site.com/" .$lang . "/digitaloutage" . "/offline";
  }
  else{
    $redirect_url = "https://client-site.com/" . $lang . "/digitaloutage";
  }
  header('Location: ' . $redirect_url);
}

?>

<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="ISO-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, user-scalable=false;">
    <title>Website Outage Notification Form</title>

    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" media="print" href="css/style_print.css">

    <script src="js/modernizr.js"></script>
    <script src="//nexus.ensighten.com/prod/Bootstrap.js"></script>
</head>

<body>
<!-- site wrapper -->
<div id="site_wrapper">

    <!-- site container -->
    <div id="site_container">

        <!-- site header -->
        <header>
            <div id="site_header_left">
                <div id="site_logo">
                    <a href="http://www.client-name.com/" title="Return to Website">
                        <img src="images/logo<?php print ($language == 'en') ? 'en' : 'fr'; ?>.gif" alt="client" />
                    </a>
                </div>
            </div>
            <div id="site_header_right">
                <a id="languageSwitcher" href="index.php?lang=<?php print ($language == 'en') ? 'fr' : 'en'; ?>"><?php print ($language == 'en') ? 'FR' : 'EN'; ?></a>
            </div>
        </header>
        <!-- / site header -->

        <!-- site content -->
        <div id="site_content">
            <div id="addPadding">
                <div class="pageHeading">
                    <h1>
                      <?php print $GLOBALS['t']->_('Get a notification when our service is back'); ?>
                    </h1>
                </div>

                <div class="addPadding">
                    <div id="content">
                      <?php
                      print '<p>' . $GLOBALS['t']->_('Just give us your email, and we\'ll let you know when we\'re up and running.  We respect your privacy. Your email address will be protected and used only to send the notification.') . '</p>';
                      ?>
                    </div>
                    <div id="emailForm">
                        <form method="post" action="index.php" id="programForm">
                            <label for="email"><?php print $GLOBALS['t']->_('Enter your email'); ?></label>
                            <div id="emailWrapper">
                                <input type="text" name="email" id="email" value="" class="<?php if($hasError){ print 'error'; } ?>"/>
                                <input type="hidden" name="language" value="<?php print $language == 'en' ? 'en' : 'fr'; ?>" />
                                <input class="formSubmit" type="submit" value="Submit" />
                            </div>
                          <?php if($hasError){ print '<span id="errors">' . $errorMessage . '</span>'; } ?>
                        </form>
                    </div>
                    <div id="grassBackground"></div>
                </div>
            </div>
        </div>
        <!-- / site content -->

    </div>
    <!-- / site container -->

</div>
<!-- / site wrapper -->

<!-- Footer -->
<footer>
    <div id="footer">
        <div class="footer-nav">
            <ul>
                <li><a href="http://about.client-name.com/community/english"><?php print $GLOBALS['t']->_("About us"); ?> </a></li>
                <li><a href="http://www.client-name.com"><?php print $GLOBALS['t']->_("client-name.com"); ?></a></li>
            </ul>
        </div>
        <div>
    </div>
</footer>
<!-- / Footer -->

<script src="js/jquery.js"></script>
<script src="js/site.js"></script>
</body>
</html>
<!-- Copyright 1998-2012 Acro Media Inc. All rights reserved -->
