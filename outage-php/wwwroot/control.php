<?php

// Include declarations for email status and $production variable.
include('config.php');

session_start();
set_time_limit(240);

require_once('includes/common.inc');
require_once('../settings.php');

// set the default time zone
date_default_timezone_set('America/Toronto');

if($_GET['logout']) {
  session_destroy();
  header("location: control.php");
}

// setup some variables
$userName = '';
$password = '';

$hasError = false;
$errorMessages = array();

$stage = 1;

if(validateLogin()) {
  $stage = 2;
  $mysqli = new mysqli($DBHOST,$DBUSER, $DBPASS, $DBNAME);
}

function validateLogin() {
  global $userName, $password, $hasError, $errorMessages;
  $correctUserName = 'user';
  $correctPassword = 'password';
  if($_SESSION['logged_in'] === true) {
    return true;
  }
  else {
    if($_POST['login']) {
      if($_POST['userName'] != $correctUserName || $_POST['password'] != $correctPassword) {
        $hasError = true;
        $userName = 'error';
        $password = 'error';
        $errorMessages[] = 'The username or password you entered is incorrect.';
        return false;
      }
    }
    else {
      return false;
    }

    $_SESSION['logged_in'] = true;
    return true;
  }
}

if($_POST['form_submitted']) {
  $form_submitted = TRUE;
}
else {
  $form_submitted = FALSE;
}

if($stage == 2) {
  $result = $mysqli->query("SELECT COUNT(id) as numResults FROM webformResults");

  $row = $result->fetch_assoc();
  $numSubmissions = $row['numResults'];
}

if($stage == 2 && $_GET['sendEmail'] && $form_submitted) {
  $cli_command = 'cd ' . realpath('.') . '; cd ../scripts; php send_email.php force';
  $cli_command .= ' > /dev/null & echo $!';
  shell_exec($cli_command);
}
if($stage == 2 && $_GET['pauseEmail'] && $form_submitted) {
  /*
   * In the case that the page hasn't refreshed or something else has happened,
   * we need to make sure they can still pause
   */
  $result = $mysqli->query("SELECT `value` FROM webformControl WHERE `setting` = 'emailQuantity'");
  $emailQuantity = $result->fetch_object()->value;

  $result = $mysqli->query("SELECT COUNT(id) as sent FROM webformResults WHERE notified = 0");
  $numLeft = $result->fetch_object()->sent;

  if($numLeft > 0){
    $result = $mysqli->query("REPLACE INTO `webformControl` (`setting`,`value`) VALUES ('emailStatus', 2)");
  }
}
if($stage == 2 && $_GET['resumeEmail'] && $form_submitted) {
  $result = $mysqli->query("REPLACE INTO `webformControl` (`setting`,`value`) VALUES ('emailStatus', 1)");
}

if($stage == 2 && $_GET['email_status_reset'] && $form_submitted) {
  // SQL TO RESET EMAIL STATUS INDICATORS
  $result = $mysqli->query("REPLACE INTO `webformControl` (`setting`,`value`) VALUES ('emailStatus', 0)");
  $result = $mysqli->query("DELETE FROM `webformControl` WHERE `setting` LIKE 'ReportLastSent'");
}

if($stage == 2 && $_GET['generate_report'] && $form_submitted) {
  $cli_command = 'cd ' . realpath('.') . '; cd ../scripts; php generate_report.php force';
  $cli_command .= ' > /dev/null & echo $!';

  shell_exec($cli_command);

  $result = $mysqli->query("REPLACE INTO `webformControl` (`setting`,`value`) VALUES ('ReportLastSent', NOW())");
  $result = $mysqli->query("REPLACE INTO `webformControl` (`setting`,`value`) VALUES ('ReportLastSentStatus', 'Sending...')");
}

if($stage == 2 && $_GET['export_data'] && $form_submitted) {
  //Correct key might be the ones in the preorder folder, not outage
  $fName = '/home/www/otuage-php/keys/webform.key';
  $f = fopen($fName, 'r');
  $key = fread($f, filesize($fName));
  require_once 'Classes/PHPExcel.php';

  $objPHPExcel = new PHPExcel();

  $objPHPExcel->getProperties()
    ->setCreator("Acro Media Inc.")
    ->setLastModifiedBy("Acro Media Inc.");


  $objPHPExcel->getProperties()->setTitle(date("F j, Y, g:i a") . ' client data');
  $columns = array('Email Address', 'Language', 'Time Submitted');

  $row = 1;
  $column = 0;
  foreach($columns as $value) {
    $column_letter = PHPExcel_Cell::stringFromColumnIndex($column);
    $cell_pointer = $column_letter . $row;
    $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_pointer, $value);
    $objPHPExcel->getActiveSheet()->getColumnDimension($column_letter)->setAutoSize(true);
    $objPHPExcel->getActiveSheet()->getStyle($cell_pointer)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $objPHPExcel->getActiveSheet()->getStyle($cell_pointer)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle($cell_pointer)->getFont()->setSize(14);
    $column++;
  }

  $row++;

  // Note that on PRODUCTION, the form input may be encrypted differently than that of STAGING: change query.
  if($production==true){
      $result = $mysqli->query("SELECT email, language, unix_timestamp(timestamp) as timestamp FROM webformResults ORDER BY id DESC");
  }
  else{
    $result = $mysqli->query("SELECT AES_DECRYPT(email, '$key'), language, unix_timestamp(timestamp) as timestamp FROM webformResults ORDER BY id DESC");
  }


  while($rowData = $result->fetch_assoc()) {
    $row++;
    $column = 0;
    foreach($rowData as $key => $value) {
      $column_letter = PHPExcel_Cell::stringFromColumnIndex($column);
      $cell_pointer = $column_letter . $row;
      if($key == 'timestamp') {
        $value = date("F j, Y, g:i a", $value);
      }
      $objPHPExcel->setActiveSheetIndex(0)->setCellValue($cell_pointer, $value);
      $objPHPExcel->getActiveSheet()->getStyle($cell_pointer)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
      $column++;
    }
  }

  $objPHPExcel->setActiveSheetIndex(0);

  ob_end_clean();

  header("Cache-Control: no-store, no-cache, must-revalidate");
  header("Cache-Control: post-check=0, pre-check=0", false);
  header("Pragma: no-cache");
  header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
  header('Content-Disposition: attachment;filename="client-data-' . date('Y-m-d') . '.xlsx"');


  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
  ob_end_clean();

  $objWriter->save('php://output');

  // Blanking out this header fixes a bug with downloading files over SSL in IE8.
  header("Pragma: ");

  $objPHPExcel->disconnectWorksheets();
  unset($objPHPExcel);

//  readfile('client_data.xsl');
//  unlink('client_data.xsl');
  exit();
}

if($stage == 2 && $_GET['turn_off'] && $form_submitted) {
  $result = $mysqli->query("REPLACE INTO `webformControl` (`setting`,`value`) VALUES ('WebformStatus', 'Off')");
}

if($stage == 2 && $_GET['turn_on'] && $form_submitted) {
  $result = $mysqli->query("REPLACE INTO `webformControl` (`setting`,`value`) VALUES ('WebformStatus', 'On')");
}

if($stage == 2 && $_GET['delete_data'] && $form_submitted) {
  $query = "DELETE FROM webformResults";
  $mysqli->query($query);
}

?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
    <meta charset="ISO-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title>Website Outage Form</title>
    <link rel="stylesheet" href="css/style_control.css">
    <script src="js/modernizr.js"></script>
    <meta http-equiv="refresh" content="300"> <!-- refresh so sending status can update -->
    <script src="js/jquery.js"></script>
</head>

<body id="layout_a">

<!-- site wrapper -->
<div id="site_wrapper">

    <!-- site container -->
    <div id="site_container">

        <!-- site header -->
        <header>
            <div id="site_header_left">
                <div id="site_logo">
                    <a href="/"><img src="images/logo.gif" alt="client-name" /></a>
                </div>
            </div>
            <div id="site_header_right">
              <?php if($stage > 1) { ?>
                  <a href="control.php?logout=true"><span class="floatRight">Log Out</span></a>
              <?php } ?>
            </div>
            <br class="clear_both" />
        </header>
        <!-- / site header -->

        <!-- site content -->
        <div id="site_content" class="control">
            <div id="addPadding">
              <?php if($stage == 1) { ?>
                  <div class="pageHeading">
                      <h1>Please Log In</h1>
                  </div>

                  <div class="addContentPadding">
                      <div class="webform_top">
                          <div class="webform_bot">
                              <div class="addPadding">
                                <?php if($hasError) { ?>
                                    <div style="width: 385px; text-align: left">
                                        <p class="errorHeading">There <?php print (count($errorMessages) > 1 ? 'were' : 'was'); ?> <?php print count($errorMessages); ?> error<?php print (count($errorMessages) > 1 ? 's' : ''); ?> with your request.</p>
                                        <div id="errorListing">
                                            <ol>
                                              <?php foreach($errorMessages as $message) { ?>
                                                  <li><?php print $message; ?></li>
                                              <?php } ?>
                                            </ol>
                                        </div>
                                    </div>
                                <?php } ?>
                                  <form action="control.php" method="POST" id="programForm">
                                      <input type="hidden" name="login" value="true" />
                                      <table>
                                          <tr>
                                              <td style="width: 200px;">
                                                  <p class="inputLabel <?php print $userName; ?>">User Name</p>
                                                  <input type="text" style="width: 150px;" name="userName" value="<?php print ($_POST['userName'] != '' ? $_POST['userName'] : ''); ?>"/>
                                              </td>
                                              <td>
                                                  <p class="inputLabel <?php print $password; ?>">Password</p>
                                                  <input type="password" style="width: 150px;" name="password" value="<?php print ($_POST['password'] != '' ? $_POST['password'] : ''); ?>" />
                                              </td>
                                          </tr>
                                      </table>
                                  </form>
                              </div>
                          </div>
                      </div>
                    <?php if($stage == 1) { ?>
                        <div style="float: right; margin-top: -4px; padding-right: 7px;"><a class="bEXP2" href="#" id="submitButton"><span>Login</span></a></div>
                    <?php } ?>
                  </div>
              <?php } else { ?>
                  <div class="pageHeading">
                      <h1>Outage Control</h1>
                  </div>
              <?php if($_GET['sendEmail']) { ?>
                  <h3>The emails are being sent.</h3>
                  <a href="control.php">Back</a>
              <?php } else if($_GET['pauseEmail'] && ($numLeft > $emailQuantity)) { ?>
                  <h3>E-Mail sending paused, any email in the current batch will still be processed.</h3>
                  <a href="control.php">Back</a>
              <?php } else if($_GET['pauseEmail'] && ($numLeft <= $emailQuantity)) { ?>
                  <h3>E-Mail cannot be paused at this time.</h3>
                  <a href="control.php">Back</a>
              <?php } else if($_GET['resumeEmail']) { ?>
                  <h3>E-Mail sending resumed.</h3>
                  <a href="control.php">Back</a>
              <?php } else if($_GET['email_status_reset']) { ?>
                  <h3>E-Mail status indicators have been reset.</h3>
                  <a href="control.php">Back</a>

              <?php } else if($_GET['generate_report'] && $form_submitted) { ?>
                  <h3>The report has been sent.</h3>
                  <a href="control.php">Back</a>

              <?php } else if($_GET['change_report_recipients'] && $form_submitted) { ?>
                  <h3>Add email addresses to the textarea below, One email per line.<br/><br/>Also please make sure there are no spaces at the end of each line and no extra line after the last email address.</h3>
              <br />
                <?php
                $result = $mysqli->query("SELECT value from `webformControl` WHERE `setting` = 'reportEmails'");
                $emails = $result->fetch_object();
                $emails = $emails->value;
                $emails = unserialize($emails);
                $emails = implode("\n", $emails);
                ?>

                  <form method="post" action="control.php">
                      <textarea name="report_emails" rows="5" cols="50"><?php print $emails; ?></textarea>
                      <br />
                      <br />
                      <input type="submit" name="save_report_recipients" value="Save">
                  </form>

              <br />
                  <a href="control.php">Back</a>

              <?php } else if($_POST['save_report_recipients']) { ?>
              <?php
              $emails = explode("\n", $_POST['report_emails']);
              $emails = array_filter(array_map('trim', $emails));

              $data = serialize($emails);
              $result = $mysqli->query("REPLACE INTO `webformControl` (`setting`, `value`) VALUES('reportEmails', '" . $data . "')");
              ?>

                  <h3>Email addresses saved.</h3>

                  <a href="control.php">Back</a>

              <?php } else if($_GET['delete_data'] && $form_submitted) { ?>
                  <h3>All client submissions have been deleted.</h3>
                  <a href="control.php">Back</a>

              <?php // don't both checking for form_submitted here. ?>
              <?php } else if($_GET['view_report']) {
              $result = $mysqli->query("SELECT COUNT(id) as submissions FROM webformResults");
              $numSubmissions = $result->fetch_object()->submissions;
              ?>
                  <font style="color:#666666; font-face: arial; font-size: 12px;">
                      <h1 style="font-size: 18px; font-weight: bold; color: #92D050; padding-bottom: 10px;">Outage report for: <?php print date("F j, Y, g:i a"); ?> (EST)</h1>
                      <table cellpadding="0" cellspacing="0" style="border-collapse: collapse;border: 1px solid #CCCCCC; padding: 5px; -moz-box-shadow: 0px 2px 2px 2p #ccc; -webkit-box-shadow: 0px 2px 2px 2px #ccc; box-shadow: 0px 2px 2px 2px #ccc; border-radius: 5px;">
                          <tr>
                              <td style="font-weight: bold; width: 150px; padding: 5px 0px 5px 10px; border-bottom: 1px solid #CCCCCC; border-collapse: collapse;">Total Client Submissions</td>
                              <td style="font-weight: bold; width: 150px; padding: 5px 0px 5px 0px; border-bottom: 1px solid #CCCCCC; border-left: 1px solid #CCCCCC; text-align: center; border-collapse: collapse;"><?php print $numSubmissions; ?></td>
                          </tr>
                      </table>
                  </font>
              <br />
                  <a href="control.php">Back</a>
              <?php } else if($_GET['email_settings'] && $form_submitted) {
              $result = $mysqli->query("SELECT `value` FROM webformControl WHERE `setting` = 'emailQuantity'");
              $emailQuantity = $result->fetch_object()->value;

              $result = $mysqli->query("SELECT `value` FROM webformControl WHERE `setting` = 'emailFrequency'");
              $emailFrequency = $result->fetch_object()->value;

              $quantity = array(500,1000,2500,5000,10000);
              $frequency = array(
                5 => '5 Minutes',
                10 => '10 Minutes',
                15 => '15 Minutes',
                30 => '30 Minutes',
                60 => '1 Hour',
              );
              ?>
                  <h3>Select the emails sent per batch and the frequency of sending.</h3>
                  <form method="post" action="control.php">
                      <label for="quantity">Quantity:</label>
                      <select name="quantity">
                        <?php
                        foreach ($quantity as $value) {
                          if($value == $emailQuantity){
                            print '<option value="'.$value.'" selected="selected">'.$value.'</option>';
                          }
                          else{
                            print '<option value="'.$value.'">'.$value.'</option>';
                          }
                        }
                        ?>
                      </select>
                      &nbsp;
                      <label for="frequency">Frequency:</label>
                      <select name="frequency">
                        <?php
                        foreach ($frequency as $value => $label) {
                          if($value == $emailFrequency){
                            print '<option value="'.$value.'" selected="selected">'.$label.'</option>';
                          }
                          else{
                            print '<option value="'.$value.'">'.$label.'</option>';
                          }
                        }
                        ?>
                      </select>
                      <br /><br />
                      <input class="formSubmit" type="submit" name="save_email_settings" value="Save">
                  </form>
              <br /><br />
                  <a href="control.php">Back</a>
              <?php } else if($_POST['save_email_settings']) {
              $quantity = $_POST['quantity'];
              $frequency = $_POST['frequency'];
              if(is_numeric($frequency) && is_numeric($quantity)){
                $result = $mysqli->query("REPLACE INTO `webformControl` (`setting`, `value`) VALUES('emailQuantity', '" . $quantity . "')");
                $result = $mysqli->query("REPLACE INTO `webformControl` (`setting`, `value`) VALUES('emailFrequency', '" . $frequency . "')");
                print '<h3>Email settings saved.</h3>';
              }
              else{
              ?>
                  <h3 class="error">ERROR: Invalid Input</h3>
              <?php } ?>
                  <a href="control.php">Back</a>
              <?php } else {
              // DEFAULT CONTROL PANEL PAGE

              $actions = array();

              $result = $mysqli->query("SELECT `value` FROM webformControl WHERE `setting` = 'emailQuantity'");
              $emailQuantity = $result->fetch_object()->value;

              $result = $mysqli->query("SELECT `value` FROM webformControl WHERE `setting` = 'emailStatus'");
              $emailStatus = $result->fetch_object()->value;

              $result = $mysqli->query("SELECT COUNT(id) as sent FROM webformResults WHERE notified = 1");
              $numSent = $result->fetch_object()->sent;

              $result = $mysqli->query("SELECT `value` FROM webformControl WHERE `setting` = 'emailLastSent'");
              $row = $result->fetch_object();
              if ($row) {
                $emailLastSent = 'Sent: ' . $row->value . ' ' . date('T');
              }
              else {
                $emailLastSent = "";
              }

              switch($emailStatus){
                case EMAIL_NOT_SENT_STATUS:
                  $numLeft = ($numSubmissions - $numSent) . ' ';
                  $statusText = $numLeft . 'Not Sent';
                  $actions[] = '<li><a href="control.php?sendEmail=true" data-confirm="Clicking OK will t '.$numLeft.' emails in batches of '.$emailQuantity.'. Are you absolutely sure you want to do this?"><span style="font-size: 12px;">Send outage over email</span></a></li>';
                  break;
                case EMAIL_SENDING_STATUS:
                  $statusText = 'Sending';
                  $subText = 'Entire Batch';
                  if($numSubmissions > $emailQuantity){
                    $max = (ceil($numSent / $emailQuantity ) * $emailQuantity) > $numSubmissions ? $numSubmissions : (ceil($numSent / $emailQuantity ) * $emailQuantity);
                    $subText = $max.' out of '.$numSubmissions;
                    $actions[] =  '<li><a href="control.php?pauseEmail=true"><span style="font-size: 12px;">Pause emails</span></a></li>';
                  }
                  break;
                case EMAIL_PAUSED_STATUS:
                  $statusText = 'Paused';
                  $max = (ceil($numSent / $emailQuantity ) * $emailQuantity) > $numSubmissions ? $numSubmissions : (ceil($numSent / $emailQuantity ) * $emailQuantity);
                  $subText = $max.' out of '.$numSubmissions;
                  $actions[] =  '<li><a href="control.php?resumeEmail=true"><span style="font-size: 12px;">Resume sending</span></a></li>';
                  break;
                case EMAIL_COMPLETE_STATUS:
                  $statusText = 'Complete';
                  $subText = $emailLastSent;
                  break;
              }

              $result = $mysqli->query("SELECT `value` FROM webformControl WHERE `setting` = 'ReportLastSent'");
              $row = $result->fetch_object();
              if ($row) {
                $result2 = $mysqli->query("SELECT `value` FROM webformControl WHERE `setting` = 'ReportLastSentStatus'");
                $row2 = $result2->fetch_object();
                $ReportLastSent = 'Sent: ' . $row->value . ' ' . date('T') . ' (' . $row2->value .  ')' ;
              }
              else {
                $ReportLastSent = "";
              }
              ?>
                  <form id="control-form" method="post" action="control.php">
                      <input type="hidden" name="form_submitted" value="true">
                      <h2>Form Options</h2>
                      <h3>There are <?php print $numSubmissions; ?> client submissions in the database.</h3>
                      <div style="padding-left: 20px;">
                          <p class="bold" style="margin-bottom: 0px;">Current Status: <span class="status<?php print $emailStatus; ?>"><?php print $statusText; ?></span>&nbsp;<span class="subText"><?php print (isset($subText)) ? '('.$subText.')' : ''; ?></span></p>
                          <ul style="list-style: disc inside none;">
                            <?php
                            foreach ($actions as $action) {
                              print $action;
                            }
                            ?>
                          </ul>
                          <p class="bold" style="margin-bottom: 0px;">Available Database actions</p>
                          <ul style="list-style: disc inside none;">
                              <li><a href="control.php?email_settings=true"><span style="font-size: 12px;">Change email quantity and frequency</span></a></li>
                              <li><a href="control.php?export_data=true"><span style="font-size: 12px;">Download client submissions</span></a></li>
                              <li><a href="control.php?delete_data=true" data-confirm="Clicking OK will delete all client submissions permanently. Are you absolutely sure you want to do this?"><span style="font-size: 12px;">Delete all client submissions</span></a></li>
                          </ul>
                      </div>

                      <h2>Reporting Options</h2>
                      <div style="padding-left: 20px;">
                          <p class="bold" style="margin-bottom: 0px;">Available E-Mail actions</p>
                          <ul style="list-style: disc inside none;">
                              <li><a href="control.php?generate_report=true"><span style="font-size: 12px;">Generate & send a report now</span></a>&nbsp;&nbsp;<span style="font-style:italic; color: #6C0;"><?php print $ReportLastSent; ?></span></li>
                              <li><a href="control.php?change_report_recipients=true"><span style="font-size: 12px;">Change Recipients</span></a></li>
                          </ul>

                          <p class="bold" style="margin-bottom: 0px;">Available actions</p>
                          <ul style="list-style: disc inside none;">
                              <li><a href="control.php?view_report=true"><span style="font-size: 12px;">View current report</span></a></li>
                          </ul>
                      </div>

                      <h2>Webform Status</h2>
                      <div style="padding-left: 20px;">
                          <p class="bold" style="margin-bottom: 0px;">Available Webform actions</p>
                          <ul style="list-style: disc inside none;">
                            <?php if(!checkToolStatus()) { ?>
                                <li><a href="control.php?turn_on=true"><span style="font-size: 12px;">Turn the webform "on"</span></a></li>
                            <?php } else { ?>
                                <li><a href="control.php?turn_off=true"><span style="font-size: 12px;">Turn the webform "off"</span></a></li>
                            <?php } ?>
                              <li><a href="control.php?email_status_reset=true" data-confirm="Clicking OK will reset the e-mail status indicators. Are you absolutely sure you want to do this?"><span style="font-size: 12px;">Reset E-Mail Status</span></a></li>

                          </ul>
                      </div>
                  </form>
                  <script>
                      $(document).ready(function() {
                          $("#control-form a").click(function(e){
                              e.preventDefault();
                              if (typeof $(this).attr('data-confirm') != 'undefined') {
                                  var confirmText = $(this).attr('data-confirm');
                                  if(!confirm(confirmText)) {
                                      return false;
                                  }
                              }
                              var action = $(this).attr('href');
                              $("#control-form").attr('action', action);
                              $("#control-form").submit();
                          });
                      });
                  </script>

              <?php } ?>

              <?php } ?>
            </div>
        </div>
        <!-- / site content -->

    </div>
    <!-- / site container -->

</div>
<!-- / site wrapper -->

<script src="js/site.js"></script>

</body>
</html>
<!-- Copyright 1998-2012 Acro Media Inc. All rights reserved -->
<?php
if($stage == 2) {
  $mysqli->close();
}
?>
