<?php
session_start();
header('Cache-control: no-store');
require_once('includes/common.inc');
require_once('includes/rfc822.inc');

// set the default time zone
date_default_timezone_set('America/Toronto');

if (strpos( $_SERVER['SERVER_NAME'], 'acrobuild' ) !== false) {
  $cookie_domain = "outage-php.client-name.com";
} else {
  $cookie_domain = ".client-name.com";

}
include_once('includes/language.inc');
?>

<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
  <head>
    <meta charset="ISO-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, user-scalable=false;">
    <title>Website Outage Notification Form</title>

    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" media="print" href="css/style_print.css">

    <script src="js/modernizr.js"></script>
    <script src="//nexus.ensighten.com/prod/Bootstrap.js"></script>
  </head>

  <body>
    <!-- site wrapper -->
    <div id="site_wrapper">

      <!-- site container -->
      <div id="site_container">

        <!-- site header -->
        <header>
          <div id="site_header_left">
            <div id="site_logo">
              <a href="http://www.client-name.com/" title="Return to client-name.com">
                <img src="images/logo<?php print ($language == 'en') ? 'en' : 'fr'; ?>.gif" alt="Telus" />
              </a>
            </div>
          </div>
          <div id="site_header_right">
          </div>
        </header>
        <!-- / site header -->

        <!-- site content -->
        <div id="site_content">
          <div id="addPadding">
            <div class="pageHeading">
                <h1>
                <?php print $GLOBALS['t']->_("Your request has been submitted"); ?>
                </h1>
            </div>

            <div class="addPadding">
              <div id="content">
                <?php
                  print '<p>' . $GLOBALS['t']->_('An email will be sent to you when we resume service.') . '</p>';
                  print '<p id="thankYou">' . $GLOBALS['t']->_('Thank you for your patience.') . '</p>';
                ?>
              </div>
              <div id="grassBackground"></div>
            </div>
          </div>
        </div>
        <!-- / site content -->

      </div>
      <!-- / site container -->

    </div>
    <!-- / site wrapper -->

    <!-- Footer -->
    <footer>
      <div id="footer">
        <div class="footer-nav">
          <ul>
            <li><a href="http://about.client-name.com/community/english"><?php print $GLOBALS['t']->_("About us"); ?> </a></li>
          </ul>
        </div>
        <div>
          <a href="http://www.client-name.com/" title="Return to client-name.com"><img src="images/footer-logo-<?php print ($language == 'en') ? 'en' : 'fr'; ?>.png" alt="client-name" /></a>
        </div>
        <div id="copyright">
          &copy;
          <?php print date('Y') . ' ' . $GLOBALS['t']->_('client-name description'); ?>
        </div>
      </div>
    </footer>
    <!-- / Footer -->

    <script src="js/jquery.js"></script>
    <script src="js/site.js"></script>
  </body>
</html>
<!-- Copyright 1998-2012 Acro Media Inc. All rights reserved -->
