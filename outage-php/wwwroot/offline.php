<?php
include_once('includes/language.inc');
require_once('includes/common.inc');

if(checkToolStatus()) {
  header('location: index.php');
}

?>

<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta charset="ISO-8859-1">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title>Website Outage Form</title>

		<link rel="stylesheet" href="css/style.css">
		<script src="js/modernizr.js"></script>
	</head>

	<body id="layout_a">

		<!-- site wrapper -->
		<div id="site_wrapper">

			<!-- site container -->
			<div id="site_container">

        <!-- site header -->
        <header>
          <div id="site_header_left">
            <div id="site_logo">
              <a href="/"><img src="images/logo<?php print ($language == 'en') ? 'fr' : 'en'; ?>.gif" alt="client-name" /></a>
            </div>
          </div>
          <div id="site_header_right">
              <a id="languageSwitcher" href="index.php?lang=<?php print ($language == 'en') ? 'fr' : 'en'; ?>"><?php print ($language == 'en') ? 'FR' : 'EN'; ?></a>
          </div>
        </header>
        <!-- / site header -->

				<!-- site content -->
				<div id="site_content" class="offline">
  				<div id="addPadding">
  					<p style="font-weight: bold;"><?php print $GLOBALS['t']->_("Thank you for your patience."); ?></p>
  					<p style="font-weight: bold;"><?php print $GLOBALS['t']->_("Services have been restored."); ?></p>
  				</div>
				</div>
				<!-- / site content -->

			</div>
			<!-- / site container -->

		</div>
		<!-- / site wrapper -->

    <!-- Footer -->
    <footer>
      <div id="footer">
        <div class="footer-nav">
          <ul>
            <li><a href="http://about.client-name.com/community/english"><?php print $GLOBALS['t']->_("About us"); ?> </a></li>
            <li><a href="http://www.client-name.com"><?php print $GLOBALS['t']->_("client-name.com"); ?></a></li>
            <li><a href="http://why.client-name.com/"><?php print $GLOBALS['t']->_("Client-name"); ?></a></li>
          </ul>
        </div>
        <div>
          <img src="images/footer-logo-<?php print ($language == 'en') ? 'fr' : 'en'; ?>.png" alt="client-name" />
        </div>
        <div id="copyright">
          &copy;
          <?php date_default_timezone_set('America/Toronto'); ?>
          <?php print date('Y') . ' ' . $GLOBALS['t']->_('client name'); ?>
        </div>
      </div>
    </footer>
    <!-- / Footer -->

    <script src="js/jquery.js"></script>
    <script src="js/site.js"></script>
  </body>
</html>
<!-- Copyright 1998-2012 Acro Media Inc. All rights reserved -->

