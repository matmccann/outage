# Website Outage Center Tool

## used to engage a email back form for customers to be alerted when an outage is over

### Setup

* Setup a `settings.local.php` file, see `example.settings.local.php`
  * This allows connection to a database, which can be hosted locally.
* Grab DB, `ssh dbslave` and grab a database dump for `outage`
* Import the sql into your database location.
* Run `php -S localhost:8000 -t wwwroot` this makes php a webserver.
  * `-S` `<addr>:<port>` Run with built-in web server.
  * `-t` `<docroot>`     Specify document root `<docroot>` for built-in web server.

  TODO Setup docker for this...

### Maintenance & Changes
- STAGING: create changes to push to UAT2
- PRODUCTION: merge changes into master
- PRODUCTION: set configuration variable in index.php, control.php, and send_email.php so that the read operations relies on outage-api encryption
```bash
$production = true;
```

### Introduction

Tool used by ```client-name``` to collect customer emails during an outage and sends out and email when the outage is over.

### Functionality

* Sends email addresses capture over to SendGrid VIA cURL
* Sends email out containing reportEmails from webFormControl table

#### `control.php`

* 1.) Login

* 2.) Display control.php

* Form Options
  * Current status
  * Change email quantity and frequency
  * Download client submissions
  * Delete all client submissions
* Reporting Options
  * Available E-Mail actions
    * Generate & send a report now  
    * Change Recipients
  * Available actions
    * View current report
* Webform Status
  * Available Webform actions
    * Turn the webform "off"
    * Reset E-Mail Status

All the files that used to be in this folder have moved:

/corporateoutage/wwwroot/* ->  /preorder/outagecentre/*
/corporateoutage/keys/*    ->  /preorder/keys/*
/corporateoutage/scripts/* ->  /preorder/scripts/*

*** POEDIT ***
  Although language translations now appear in the preorder site's 
  PO file, you can probably ignore those. The real PO
  file is in the outagecentre's subfolder... that's the one
  you need to be updating.
