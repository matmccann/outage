# services/users/project/api/users.py

from flask import Blueprint, request, render_template, jsonify

from project import db
from project.api.models import User


""" CREATE BLUEPRINT FOR THE APPLICATION """


users_blueprint = Blueprint('api', __name__, template_folder='./templates')


""" FUNCTION FOR CRUD OPERATION """


def update_fields(update, user, response_object):

    if("email" in update):
        user.email = update["email"]
        response_object['status'] += ', email'

    if("mobileNumber" in update):
        user.mobileNumber = update["mobileNumber"]
        response_object['status'] += ', mobileNumber'

    if("language" in update):
        user.language = update["language"]
        response_object['status'] += ', language'

    if("active" in update):
        user.active = update["active"]
        response_object['status'] += ', active'


""" THIS IS A TEST PAGE THAT MUST BE ENABLED FOR THE TEST RUNNER """
@users_blueprint.route('/testpage', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        email = request.form['email']
        db.session.add(User(email=email))
        db.session.commit()
    users = User.query.all()
    return render_template('index.html', users=users)


""" THIS IS THE DEFAULT CUSTOMER PAGE """
@users_blueprint.route('/api/email', methods=['GET'])
def showClientPage():
    """ Looks at api/template folder """
    supported_languages = ["en", "fr"]
    lang = request.accept_languages.best_match(supported_languages)
    if(lang == 'fr'):
        return render_template('TEMPLATE_FR.html') 
    else:
        return render_template('TEMPLATE.html')


""" TESTING CONNECTIVITY """
@users_blueprint.route('/api/ping', methods=['GET'])
def ping():
    response_object = {
        'status': 'success',
        'message': 'pong!'
    }
    return jsonify(response_object), 200


""" CRUD OPERATIONS """


@users_blueprint.route('/api/create', methods=['GET', 'POST'])
def create():
    if request.method == 'GET':
        response_object = {
            'status': 'fail',
            'message': 'Invalid path.'
        }
        return jsonify(response_object), 400

    if request.method == 'POST':
        post_data = request.get_json()
        response_object = {
            'status': 'fail',
            'message': 'Invalid payload.'
        }
        if not post_data:
            return jsonify(response_object), 400

        email = post_data.get('email')

        if email is None:
            return jsonify(response_object), 400

        try:
            user = User.query.filter_by(email=email).first()
            if not user:
                db.session.add(User(email=email))
                db.session.commit()
                response_object['status'] = 'success'
                response_object['message'] = f'{email} was added!'
                return jsonify(response_object), 201
            else:
                response_object['message'] = 'Sorry. That email already exists.'
                return jsonify(response_object), 400

        except:
            print('Error writing to database')
            db.session.rollback()
            response_object = {
                'status': 'fail',
                'message': 'Error writing to database.'
            }
            return jsonify(response_object), 400


@users_blueprint.route('/api/read', methods=['GET', 'POST'])
def read():

    if request.method == 'GET':
        """Get all users"""
        response_object = {
            'status': 'testing success',
            'data': {
                'users': [user.to_json() for user in User.query.all()]
            }
        }
        return jsonify(response_object), 200

    if request.method == 'POST':
        response_object = {
            'status': 'fail',
            'message': 'Invalid path.'
        }
        return jsonify(response_object), 400


@users_blueprint.route('/api/update', methods=['GET', 'POST'])
def update():

    if request.method == 'GET':
        response_object = {
            'status': 'fail',
            'message': 'Invalid path.'
        }
        return jsonify(response_object), 400

    if request.method == 'POST':
        post_data = request.get_json()
        response_object = {
            'status': 'fail',
            'message': 'Invalid payload.'
        }
        if not post_data:
            return jsonify(response_object), 400

        email = post_data.get('email')
        update = post_data.get('update')

        if email is None:
            return jsonify(response_object), 400

        try:
            user = User.query.filter_by(email=email).first()
            if user:
                response_object['status'] = 'success'
                # map field request to update field
                update_fields(update, user, response_object)
                db.session.commit()
                response_object['message'] = f'{update}'
                return jsonify(response_object), 201

            else:
                response_object['message'] = 'Update request failed.'
                return jsonify(response_object), 400

        except:
            response_object['message'] = 'Sorry. Error!'
            return jsonify(response_object), 400


@users_blueprint.route('/api/delete', methods=['GET', 'POST'])
def delete():

    if request.method == 'GET':
        response_object = {
            'status': 'fail',
            'message': 'Invalid path.'
        }
        return jsonify(response_object), 400

    if request.method == 'POST':
        post_data = request.get_json()
        response_object = {
            'status': 'fail',
            'message': 'Invalid payload.'
        }
        if not post_data:
            return jsonify(response_object), 400

        email = post_data.get('email')

        if email is None:
            return jsonify(response_object), 400

        user = User.query.filter_by(email=email).first()
        if user:
            try:
                db.session.delete(user)
                db.session.commit()
                response_object['status'] = 'success'
                response_object['message'] = f'deleted user: {user.email}'
                return jsonify(response_object), 201
            except:
                print('Error delete item from database')
                db.session.rollback()
                response_object = {
                    'status': 'fail',
                    'message': 'Error deleting item from database.'
                }
                return jsonify(response_object), 400

        else:
            response_object['message'] = 'User email does not exist.'
            return jsonify(response_object), 400
