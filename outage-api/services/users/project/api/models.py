# services/users/project/api/models.py


from sqlalchemy.sql import func

from project import db


class User(db.Model):
    # Log into mysql and find the name of the table you want to write to!
    # e.g. mysql; -> use corporateOutage; -> show tables;
    __tablename__ = 'webformResults'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    email = db.Column(db.VARCHAR(255), unique=True, nullable=False)
    language = db.Column(db.VARCHAR(2), default="en")
    timestamp = db.Column(db.DateTime, default=func.now(), nullable=False)
    notified = db.Column(db.Integer, default=0)
    phone = db.Column(db.VARCHAR(255))

    def __init__(self, email):
        self.email = email

    def to_json(self):
        return {
            'id': self.id,
            'email': self.email,
            'language': self.language,
            'notified': self.notified,
            'phone': self.phone
        }
