# services/users/project/tests/test_users.py


import json
import unittest

from project.tests.base import BaseTestCase
from project import db
from project.api.models import User


def add_user(email):
    db.session.add(User(email=email))
    db.session.commit()
    return user


class TestUserService(BaseTestCase):
    """Tests for the Users Service."""

    def test_users(self):
        """Ensure the /ping route behaves correctly."""
        response = self.client.get('api/ping')
        data = json.loads(response.data.decode())
        self.assertEqual(response.status_code, 200)
        self.assertIn('pong!', data['message'])
        self.assertIn('success', data['status'])


    # def test_add_user_invalid_json(self):
    #     """Ensure error is thrown if the JSON object is empty."""
    #     with self.client:
    #         response = self.client.post(
    #             'api/create',
    #             data=json.dumps({}),
    #             content_type='application/json',
    #         )
    #         data = json.loads(response.data.decode())
    #         self.assertEqual(response.status_code, 400)
    #         self.assertIn('Invalid payload.', data['message'])
    #         self.assertIn('fail', data['status'])


    # def test_all_users(self):
    #     """Ensure get all users behaves correctly."""
    #     add_user('mmccann@acromedia.com')
    #     add_user('billyjoe@notreal.com')
    #     with self.client:
    #         response = self.client.get('api/read')
    #         data = json.loads(response.data.decode())
    #         self.assertEqual(response.status_code, 200)
    #         self.assertEqual(len(data['data']['users']), 2)
    #         self.assertIn(
    #             'mmccann@acromedia.com', data['data']['users'][0]['email'])
    #         self.assertIn(
    #             'billyjoe@notreal.com', data['data']['users'][1]['email'])
    #         self.assertIn('success', data['status'])


if __name__ == '__main__':
    unittest.main()
