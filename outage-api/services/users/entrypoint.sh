#!/bin/sh

echo "Waiting for MariaDB..."

# MATCH: look at docker-compose.yml 'services' and insert 'users-db' or your database name AND the '3306 or your port.
while ! nc -z users-db 3306; do
  sleep 0.1
done

echo "MariaDB started"

# Name of the main python file that runs the application.
python manage.py run -h 0.0.0.0