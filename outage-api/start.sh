echo
echo "---------------------"
echo step 1: installing PYTHON dependencies
sudo apt-get update
sudo apt-get install \
    python3-pip \
    python3-venv \
    curl
echo
echo
echo "---------------------"

echo step 2: installing DOCKER dependencies
echo download and verify docker depencies
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

echo "---------------------"
echo verify fingerprints - docker
sudo apt-key fingerprint 0EBFCD88

echo "---------------------"
echo
echo
echo "---------------------"
echo step 3: installing DOCKER
echo get docker image release - intial installation
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
echo

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
sudo curl -L "https://github.com/docker/compose/releases/download/1.24.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

echo "---------------------"
docker-compose --version
echo "---------------------"

echo
echo
echo "---------------------"

echo step 4: Giving file permissions
echo
cd services/users && sudo chmod 755 entrypoint.sh
cd ../..
echo "---------------------"

echo step 5: Star-Up DOCKER
sudo docker-compose up -d --build
echo
echo
echo "---------------------"


echo step 6: Initiate MARIADB database
sudo docker-compose exec users python3 manage.py recreate_db
echo
sudo docker-compose exec users python3 manage.py seed_db
echo
echo "---------------------"

echo step 7: Run TESTS
sudo docker-compose exec users python3 manage.py test
echo
echo
echo "---------------------"
