echo
echo "---------------------"
echo step 1: shut-down DOCKER and keep images
sudo docker-compose down
echo
echo "---------------------"
echo step 3: VIEW ALL CONTAINERS: $docker ps -a
sudo docker ps -a
echo
echo "---------------------"
echo step 3: CONFIRM NO CONTAINERS: $docker ps
sudo docker ps
echo