# Simple Restful-Api with Docker, Flask, and MariaDB
- a simple template for project setup
- view application in localhost:5001/api/ping or localhost:5001/api/users or localhost5001/

## API Chart
```bash
Endpoint        HTTP Method      CRUD Method      Result
-----------------------------------------------------------------
api/ping        GET             READ            testing application connectivity
api/create	    POST            CREATE          add a user
api/read        GET             READ            read basic information from all users
api/update      POST            UPDATE          update parameter(s) for a single user; select user by email
api/delete      POST            DELETE          delete a single user and all its information; select user by email
api/email       POST            NONE            view TEMPLATE.html email template in web-browser

```
<br />
<br />
<br />
<br />


<br />
<br />
<br />
<br />

# 1. Project Setup (**DO NOT SKIP**)
*Check entrypoint.sh and start.sh and stop.sh in their appropriate folders for permissions. DO FOR EACH FILE!*
```bash
$ sudo chmod 755 entrypoint.sh
$ sudo chown $USER entrypoint.sh && sudo chgrp $USER entrypoint.sh
```
*Install python libraries*
```
$ cd services/users
$ which python3
[ if there is no output then try python3 --version]

$ python3 -m venv venv
$ source venv/bin/activate
(venv) $ pip install -r requirements.txt
```


# 2. (a) Docker Setup (if not using this then skip to "Server Setup")

1. Complete *Project Setup* and check ports

- if the port is in use then kill it (refer to Docker-compose error for 'users-db' at bottom of this file).
```bash
$ sudo netstat -tlpn | grep 3306 
tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      1045/mysqld  
$ kill 1045
```

2. Spin up docker project **in root directory** (same folder as docker-compose.yml)

```bash
$ docker-compose up -d --build
```

3. Set-up database (do this root): 
```bash
$ sudo docker-compose exec users python manage.py recreate_db
```

4. Put information in database (do this in root): 
```bash 
$ sudo docker-compose exec users python manage.py seed_db
```

5. Run the test suite (if DATABASE_URL_TEST is not declared in .env, this won't work).
```bash
$ sudo docker-compose exec users python manage.py test
```

6. Now you can revert step 5 if you'd like.  Go to http://localhost:5001/api/email and view the basic page.  Try http://localhost:5001/ping

7. Refer to **Testing the Database with Postman** section for downloadable postman API

8. Refer to **Useful Docker Commands** for running tests, login to database, and more.


# 2. (b) Server Setup
- did you complete Project Setup? If no, then you know what to do!
- do you see (venv) in front of your $? Complete "Project Setup!"
```bash
$ pip list
[ this is an output of all your project dependencies]
```
- you may have to add dependencies to requirements.txt based on the environment you are running on!

## SSH Into the Server and login to mysql
- navigate to a settings.php to find database.
- take note of the following:
```bash
'driver' => 'mysql',
 *   'database' => 'outage_prod',
 *   'username' => 'user',
 *   'password' => 'password',
 *   'host' => 'localhost',
 *   'port' => 3306,

*Configuration of this app to an existing database requires change to connection URI format*
*This URI format is in the .env file (environment variables), docker-compose.yml (general calls), and model.py (database table name)

*Example from a repository code (found in .env file in root directory)*
- 1st URI for dev environment when run in docker container (refer to docker-compose.yml service name = users-db)
- DATABASE_URL=mysql://root@users-db:3306/outage_dev
- DATABASE_URL=dialect+driver://username:password@host:port/database
- probable server solution: driver=mysql://user@localhost:3306/outage_prod
- create a DATABASE_URL_PROD=<whatever you figure out> and save it for later.

*Read the docs*
- https://flask-sqlalchemy.palletsprojects.com/en/2.x/config/
```

- login to mysql and verify table name
```bash
$ mysql -u $user -p
> password: $password

[mariadb]> show databases;
[mariadb]> use outage_prod;

[mariadb]> show tables;
[mariadb]> use webResultant;
```

- Let's update services/users/project/api/models.py and change "_ _ table_name _ _" based on what want from "show tables;"

## Export your environment variables on the server
- get in same directory as manage.py
- set FLASK variable for the app and configurations for config.py (config.py gets called by in _ _ init_ _ .py)
```bash
$ cd services/users
$ export FLASK_ENV=production && export APP_SETTINGS=project.config.ProductionConfig && export SECRET_KEY=my_precious
$ export DATABASE_URL_PROD=<whatever you figured out from above!>
```
- now run the app!
```bash
$ python3.6 manage.py run -h 127.0.0.1 &
```

- now go to "Testing the Database with Postman" section and test the api
- you may have to change the URL in postman!

## Cannot Run it on a Server
- you may have to add to requirements.txt (check part 1 above) then run ...
```bash
$ cd services/users
$ pip install -r requirements.txt
```

- The app won't start.  Do you get this error?
```bash
$ python3.6 manage.py run -h 127.0.0.1 &
<large error output with this as last line>
OSError: [Errno 98] Address already in use
```

- let's stop the python app and then try the same command (replace 8934 with YOUR PID)

```bash 
$ netstat -lpn | grep python
tcp        0      0 127.0.0.1:5000              0.0.0.0:*                   LISTEN      8934/python3.6 
$ kill -9 8934
$ python3.6 manage.py run -h 127.0.0.1 &
```

# 3. Testing the Database with Postman
- ensure that docker-compose up has the application running
```bash
1. go to https://www.getpostman.com/products to download postman for free
2. open postman and click 'import' on the top bar
3. choose import from link and paste link : https://www.getpostman.com/collections/c870e49ac094f8a08fbb
5. match the URL for each card so that it matches your destination (localhost:5001 or dev4.acromedia.com:5001)
6. try the /ping route and and expect the message "pong!"
```

# 4. Extras
- ensure that docker-compose up has the application running

# Useful Docker Commands
- ensure you are in project root directory
```bash
# Log into MariaDB
$ sudo docker-compose exec users-db mysql

# Run tests through CLI()
$ sudo docker-compose exec users python manage.py test	

# Log into Flask shell 
$ sudo docker-compose exec users flask shell
> app
> db

# recreate database
$ sudo docker-compose exec users python manage.py recreate_db

# poopulate database with dummy data
$ sudo docker-compose exec users python manage.py seed_db

# SSH into running docker container (windows)
*Show running containers, find your mongo container and grab the first 3 <container_id> characters
$ docker ps
- use winpty for windows the 2nd command for linux
$ winpty docker container exec -it <id> bash
$ docker exec -it <id> /bin/bash
```


# Start-up in Docker Container
- this start-up is environment dependent, and made for linux only!
```bash
$ sudo chmod 755 start.sh && sudo chmod 755 stop.sh
```
- now run the script and start the service for your dev server (modify/use enviroment variable "export_environment_variables)
```bash
$ ./start.sh
```

- shut down your docker project.
```bash
$ ./stop.sh
```

# 5. Troubleshooting

## Docker-compose error for 'users-db'
```bash 
Starting users_db ... error

ERROR: for users_db  Cannot start service users-db: driver failed programming external connectivity on endpoint users_db (3c6410a581d8795c404702fe36eee35c3be63e64c593301476a9204a9359bdc0): Error starting userland proxy: listen tcp 0.0.0.0:3306: bind: address already in use

ERROR: for users-db  Cannot start service users-db: driver failed programming external connectivity on endpoint users_db (3c6410a581d8795c404702fe36eee35c3be63e64c593301476a9204a9359bdc0): Error starting userland proxy: listen tcp 0.0.0.0:3306: bind: address already in use
```
- open a new bash terminal and remove any existing connections so that your new docker network functions
- this gives the ID so we can kill the process and free it up.
```bash
$ sudo netstat -tlpn | grep 3306 
tcp        0      0 127.0.0.1:3306          0.0.0.0:*               LISTEN      991/mysqld
$ kill 991
```

## Server docker-compose commands not working?
- this may be due to the version of python that you are running

```bash 
mmccann@tdev3:/home/outage$ sudo docker-compose exec users python3 manage.py recreate_db
```
<output>
python: can't open file 'manage.py': [Errno 2] No such file or directory

- check your version of python
```bash
$ which python
$ python --version
$ python3 --version
```
- change *python3* to *python* and it should work for you.
```bash
mmccann@tdev3:/home/outage$ sudo docker-compose exec users python manage.py recreate_db
```

- does your service in docker-compose.yml match what your calling? perhaps "users" is actually "users_1"?
```bash
$ docker ps 
<look at container name>
```
